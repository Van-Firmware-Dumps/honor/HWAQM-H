#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_generic_a15.mk

COMMON_LUNCH_CHOICES := \
    lineage_generic_a15-user \
    lineage_generic_a15-userdebug \
    lineage_generic_a15-eng
